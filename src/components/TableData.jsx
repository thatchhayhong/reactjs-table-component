import React from "react";
import { Container, Table } from "react-bootstrap";

function TableData(props) {
    let filteredData= props.items.filter(item=>item.total>0);
  return (
    <Container>
      <h1>Table</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Amount</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {filteredData.map((item, idx) => (
            <tr key={idx}>
              <td>{idx+1}</td>
              <td>{item.title}</td>
              <td>{item.price}</td>
              <td>{item.amount}</td>
              <td>$ {item.total} </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
}

export default TableData;
