import "./App.css";
import NavMenu from "./components/NavMenu";
import React, { Component } from "react";
import Content from "./components/Content";
import TableData from "./components/TableData";
import { Button, Badge,Container,Row,Col } from "react-bootstrap";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {
          id: 1,
          title: "Pizza",
          amount: 0,
          img: "image/pizza.jpg",
          price: 15,
          total: 0,
        },
        {
          id: 2,
          title: "Chicken",
          amount: 0,
          img: "image/chicken.jpg",
          price: 5,
          total: 0,
        },
        {
          id: 3,
          title: "Burgur",
          amount: 0,
          img: "image/burger.jpeg",
          price: 6,
          total: 0,
        },
        {
          id: 4,
          title: "Coca Cola",
          amount: 0,
          img: "image/coca.jpg",
          price: 2,
          total: 0,
        },
      ],
      selectData: [],
    };
    this.onAdd = this.onAdd.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }
  onReset(data) {
    var i;
    let newData =[];
    for (i = 0; i < data.length; i++) {
      let doc={
          id: i,
          title: data[i].title,
          amount:  data[i].amount=0,
          img:  data[i].img,
          price: data[i].price,
          total:  data[i].total=0,
      }
      newData.push(doc)
      this.setState({
        data:newData
      })
    }
  }
  onAdd(index) {
    let data = this.state.data;
    data[index].amount++;
    data[index].total = data[index].amount * data[index].price;
    this.setState({
      data: data,
    });
  }
  onDelete(index) {
    let data = this.state.data;
    data[index].amount--;
    data[index].total = data[index].amount * data[index].price;
    this.setState({
      data: data,
    });
  }

  render() {
    let filteredData=this.state.data.filter(item=>item.total>0);
    return (
      <div>
        <NavMenu />
        <Container>
        <Content
          onAdd={this.onAdd}
          onDelete={this.onDelete}
          items={this.state.data}
        />
        <Col className="p-5">
        <Row>  
        <Button variant="success" onClick={() => this.onReset(this.state.data)}>
          Reset
        </Button>
        <Badge variant="warning">{filteredData.length}</Badge></Row>
        </Col>
        
        <TableData items={this.state.data} />
          </Container>
       
      </div>
    );
  }
}
